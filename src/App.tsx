import { Component, createSignal, For, Show, JSX, createMemo, onCleanup } from "solid-js"
import { createStore, produce } from "solid-js/store"

interface ITodo {
  id: number
  title: string
  completed: boolean
}

type Mode = "all" | "active" | "completed"

let counter = 0
const ENTER_KEY = "Enter"
const ESCAPE_KEY = "Escape"

const App: Component = () => {
  const [todos, setTodos] = createStore<ITodo[]>([])
  const [showMode, setShowMode] = createSignal<Mode>("all")

  const remainingTodoCount = createMemo(() => {
    return todos.filter((todo) => !todo.completed).length
  })

  const handleAddTodo: JSX.EventHandlerUnion<HTMLInputElement, KeyboardEvent> = (e) => {
    const title = e.currentTarget.value.trim()
    if (e.key === ENTER_KEY && title) {
      const newTodos = produce<ITodo[]>((todos) => {
        todos.push({ id: Date.now(), title, completed: false })
      })
      setTodos(newTodos)
      e.currentTarget.value = ""
    }
  }

  function handleToggleTodo(todoID: number): void {
    setTodos(
      (todo) => todo.id === todoID,
      "completed",
      (completed) => !completed
    )
  }

  const handleRemoveTodo: (id: number) => JSX.EventHandlerUnion<HTMLButtonElement, MouseEvent> =
    (todoID: number) => () => {
      setTodos((todos) => todos.filter((todo) => todo.id !== todoID))
    }

  const handleToggleAll: JSX.InputEventHandlerUnion<HTMLInputElement, InputEvent> = (e) => {
    const checked = e.currentTarget.checked
    setTodos(
      () => true,
      "completed",
      () => checked
    )
  }

  const filterTodos = (todos: ITodo[]) => {
    return showMode() === "active"
      ? todos.filter((todo) => !todo.completed)
      : showMode() === "completed"
      ? todos.filter((todo) => todo.completed)
      : todos
  }

  const handleClearCompletedTodos: JSX.EventHandlerUnion<HTMLButtonElement, MouseEvent> = () => {
    setTodos((todos) => todos.filter((todo) => !todo.completed))
  }

  const handleLocation = () => {
    const location = window.location.hash.replace(/#\/?/, "") as Mode
    setShowMode(location || "all")
  }

  window.addEventListener("hashchange", handleLocation)
  onCleanup(() => window.removeEventListener("hashchange", handleLocation))

  return (
    <section class="todoapp">
      <header class="header">
        <h1>Todos</h1>
        <input
          type="text"
          class="new-todo"
          placeholder="What needs to be done?"
          onKeyDown={handleAddTodo}
        />
      </header>
      <Show when={todos.length > 0}>
        <section class="main">
          <input
            id="toggle-all"
            type="checkbox"
            class="toggle-all"
            checked={remainingTodoCount() === 0}
            onInput={handleToggleAll}
          />
          <label for="toggle-all" />
          <ul class="todo-list">
            <For each={filterTodos(todos)}>
              {(todo) => {
                return (
                  <li class="todo" classList={{ completed: todo.completed }}>
                    <div class="view">
                      <input
                        id={String(todo.id)}
                        type="checkbox"
                        class="toggle"
                        checked={todo.completed}
                        onInput={[handleToggleTodo, todo.id]}
                      />
                      <label for={String(todo.id)}>{todo.title}</label>
                      <button
                        class="destroy"
                        aria-label="Remove Todo"
                        onClick={handleRemoveTodo(todo.id)}
                      />
                    </div>
                  </li>
                )
              }}
            </For>
          </ul>
        </section>
        <footer class="footer">
          <span class="todo-count">
            <strong>{remainingTodoCount()}</strong>
            {remainingTodoCount() === 1 ? " item" : " items"} left
          </span>
          <ul class="filters">
            <li>
              <a href="#/" classList={{ selected: showMode() === "all" }}>
                All
              </a>
            </li>
            <li>
              <a href="#/active" classList={{ selected: showMode() === "active" }}>
                Active
              </a>
            </li>
            <li>
              <a href="#/completed" classList={{ selected: showMode() === "completed" }}>
                Completed
              </a>
            </li>
          </ul>
          <Show when={todos.length !== remainingTodoCount()}>
            <button class="clear-completed" onClick={handleClearCompletedTodos}>
              Clear Completed
            </button>
          </Show>
        </footer>
      </Show>
    </section>
  )
}

export default App
